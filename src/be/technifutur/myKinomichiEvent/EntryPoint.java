package be.technifutur.myKinomichiEvent;

import be.technifutur.myKinomichiEvent.Collection.ActivityCollection;
import be.technifutur.myKinomichiEvent.Collection.RegistrationCollection;
import be.technifutur.myKinomichiEvent.Menu.Menu;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;
import be.technifutur.myKinomichiEvent.View.MenuView;

public class EntryPoint {
	
	public static void main(String[] args) {
		
		// set test data
		RegistrationCollection.load();
		ActivityCollection.load();
		
		// go to initial menu view
		EntryPoint.goToMenu();
	}

	/**
	 * Show menu, ask user choice, and redirect.
	 */
	public static void goToMenu() {
		// Set menu vue and display it
		MenuView.show();

		// get choice and redirect
		int choice = UserEntries.getUserEntryByScan("Select your choice : ", Menu.MAX_MENU_SIZE);
		if(choice == 0) {
			UserEntries.scanner.close();
		}
		
		// redirect user
		Menu.redirectUser(choice);
	}
}
