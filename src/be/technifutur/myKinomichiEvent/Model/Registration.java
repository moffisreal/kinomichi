package be.technifutur.myKinomichiEvent.Model;

import be.technifutur.myKinomichiEvent.Model.Person;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;
import be.technifutur.myKinomichiEvent.View.ActivityView;
import be.technifutur.myKinomichiEvent.View.RegistrationView;

import java.util.ArrayList;
import java.util.Map;

import be.technifutur.myKinomichiEvent.Collection.ActivityCollection;
import be.technifutur.myKinomichiEvent.Collection.RegistrationCollection;
import be.technifutur.myKinomichiEvent.Menu.Menu;

/**
 * 
 * @author studentsc02
 *
 */
public class Registration {

	private static int count = 0;

	public static int getCount() {
		return count;
	}

	public static void setCount(int value) {
		count = value;
	}

	/**
	 * Add a new registrations entry
	 * @throws Exception 
	 */
	public void create() throws Exception {
		// create ref person
		Person referencePerson = new Person();
		referencePerson.create(true);

		// register registration
		createNewEntry(referencePerson, null);

		// ask user if he wants to add more person to the registration
		this.addInExisting(referencePerson, true);
	}

	/**
	 * Update a registration retreived from the person
	 * 
	 * @param person
	 * @param isFresh
	 * @throws Exception 
	 */
	public void update(Person person) throws Exception {

		// two choices :
		RegistrationView.showUpdateChoices();
		int choice = UserEntries.getUserEntryByScan("", 2);
		switch (choice) {
		case 1: // update existing

			// get activities for one person
			Map<Person, ArrayList<Activity>> registrations = RegistrationCollection.getRegistrationByReference(person);
			person = RegistrationCollection.askPersonInRegistrations(registrations);

			// show menu of registration update
			ActivityView.showUpdateChoices();
			choice = UserEntries.getUserEntryByScan("", 2);
			switch (choice) {
			case 1: // update activity in list
				System.out.println("TODO : update activity in existing list");
				break;
			case 2: // add activity in list
				System.out.println("TODO : add activity in existing list");
				break;
			}

			break;
		case 2: // add in existing registration

			this.addInExisting(person, true);

			break;
		}
	}

	/**
	 * Add a Person Activity list value in an existing registration by name
	 * 
	 * @param person
	 * @throws Exception 
	 */
	public void addInExisting(Person person, boolean askToCreateNewPerson) throws Exception {

		Person newPerson = null;
		System.out.println("Do you want to add a registration for a new person?");

		boolean add = UserEntries.getUserEntryByScan();
		if (!add) {
			System.out.println("-- Operation cancelled. --");
		}

		while (add) {

			if (askToCreateNewPerson) {
				newPerson = new Person();
				newPerson.create(false);
			}

			createNewEntry(person, newPerson);
			System.out.println("Do add a new person again ?");
			add = UserEntries.getUserEntryByScan();
		}
	}

	/**
	 * Create a Map with person as key and a list of activities as value
	 * 
	 * @param person
	 */
	public static void createNewEntry(Person referencePerson, Person newPerson) {

		Person person;
		Map<Person, ArrayList<Activity>> entry;

		// create activity collection
		ActivityCollection activityCollection = new ActivityCollection();
		activityCollection.createActivityCollection();

		// insert person / activities match
		RegistrationCollection registrationCollection = new RegistrationCollection();
		// check if newPerson exists, it means that it is an update of existing entry
		person = newPerson == null ? referencePerson : newPerson;

		// get previous registration, and update it
		if (newPerson != null) {
			entry = RegistrationCollection.getRegistrationByReference(referencePerson);
			entry.put(newPerson, activityCollection.getActivityCollection());
		} else { // createe new registration
			registrationCollection.createRegistrationCollection(person, activityCollection.getActivityCollection());
			entry = registrationCollection.getRegistration();
		}

		// add this registration to reference person
		RegistrationCollection.addToRegistrations(referencePerson, entry);

		// show success message
		RegistrationView.addedSuccess();
	}
}
