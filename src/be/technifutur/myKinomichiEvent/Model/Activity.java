package be.technifutur.myKinomichiEvent.Model;

import java.io.Serializable;

import be.technifutur.myKinomichiEvent.Collection.ActivityCollection;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;

/**
 * 
 * @author studentsc02
 *
 */
public class Activity implements Serializable{

	private static final long serialVersionUID = 1L;

	final int MAX_QUANTITY = 50;
	
	private double price;
	private int maxQuantity;
	private int quantity;
	private String name;
	private boolean isAvailable = true;
	
	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(int maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	
	// TODO : make the names unique, warn user if it already exists
	public void schedule() {
		
		// show person fields
		String message = "Enter name of activity";
		String name = UserEntries.getUserEntryByScan(message, true);
		message = "Enter activity price per unit:";
		float price = UserEntries.getUserEntryByScan(message);
		message = "Enter max quantity available per activity:";
		int maxQuantity = UserEntries.getUserEntryByScan(message, this.MAX_QUANTITY);

		
		this.setPrice(price);
		this.setName(name);
		this.setMaxQuantity(maxQuantity);
		
		// add this in collections
		ActivityCollection.addScheduled(this.getName(),this);
	}
	
	/**
	 * Update an existing scheduled activity
	 */
	public void updateScheduled() {
		
		// show person fields
		System.out.println("Current name: "+ this.getName());
		String name = UserEntries.getUserEntryByScan("Enter name of activity", true);
		System.out.println("Current activity price per unit: "+ this.getPrice());
		float price = UserEntries.getUserEntryByScan("Enter new activity price per unit: ");
		System.out.println("Current max quantity: "+ this.getMaxQuantity());
		int maxQuantity = UserEntries.getUserEntryByScan("Enter max quantity available: ", this.MAX_QUANTITY);

		String previousName = this.name;
		
		this.setPrice(price);
		this.setName(name);
		this.setMaxQuantity(maxQuantity);
		
		// update this in collection
		ActivityCollection.updateScheduled(previousName, name, this);
	}
	
	/**
	 * Update a registered activity
	 */
	public Activity update() {
			
		// show person fields
		System.out.println("Current quantity: "+ this.getQuantity());
		int quantity = UserEntries.getUserEntryByScan("Enter quantity you need: ", this.MAX_QUANTITY);
		this.setQuantity(quantity);
		
		// update this in collection
		return this;
	}

	/**
	 * Create an activity from another
	 * 
	 * @param existingScheduledActivity
	 * @param quantity
	 * @return Activity newActivity
	 */
	public static Activity createFromScheduledActivity(Activity existingScheduledActivity, int quantity) {
		
		Activity newActivity = new Activity();
		
		newActivity.setName(existingScheduledActivity.getName());
		newActivity.setPrice(existingScheduledActivity.getPrice());
		newActivity.setMaxQuantity(existingScheduledActivity.getMaxQuantity());
		newActivity.setQuantity(quantity);
		
		return newActivity;
	}
}
