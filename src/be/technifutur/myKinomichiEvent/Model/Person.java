package be.technifutur.myKinomichiEvent.Model;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Set;

import be.technifutur.myKinomichiEvent.Collection.RegistrationCollection;
import be.technifutur.myKinomichiEvent.Utils.Listing;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;

/**
 * 
 * @author studentsc02
 *
 */
public class Person implements Comparable<Person>, Serializable{

	private static final long serialVersionUID = 1L;
	
	final static String STATUS_INSTRUCTOR = "Instructor";
	final static String STATUS_PARTICIPANT = "Participant";
	
	final static String DISCOUNT_NONE = "None";
	final static String DISCOUNT_PAIR = "Pair";
	final static String DISCOUNT_UNEMPLOYED = "Unemployed";
	
	final static String[] allowedStatuses = {
			STATUS_INSTRUCTOR,
			STATUS_PARTICIPANT
			};
	
	final static String[] allowedDiscountType = {
			DISCOUNT_PAIR,
			DISCOUNT_UNEMPLOYED,
			DISCOUNT_NONE
			};
	
	private String lastName;
	private String firstName;
	private String email;
	private String phoneNumber;
	private String status = STATUS_PARTICIPANT;
	private String discount = DISCOUNT_NONE;

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	public int compareTo(Person o) {
		return this.lastName.compareTo(o.getLastName());
	}

	/**
	 * Add a person, it can be an empty person.
	 * 
	 * @param isFullyDefined
	 * @throws Exception 
	 */
	public void create(boolean isFullyDefined) throws Exception { // TODO : refactor mlettre controlleur

		// show person fields
		String message = "Enter user First name";
		String firstName = UserEntries.getUserEntryByScan(message, isFullyDefined);
		message = "Enter user Last name";
		String lastName = UserEntries.getUserEntryByScan(message, isFullyDefined);
		
		// check if a registration already exists with this user
		RegistrationCollection.registrationExists(firstName, lastName);
		
		message = "Enter user Email";
		String email = UserEntries.getUserEntryByScan(message, isFullyDefined);
		message = "Enter user Phone number";
		String phoneNumber = UserEntries.getUserEntryByScan(message, false);
		
		// show statuses field
		String[] statuses = Person.allowedStatuses;
		Listing.displayChoices(statuses);
		message = "Select the type of status of this person.";
		String status = statuses[UserEntries.getUserEntryByScan(message, statuses.length)-1];
		
		// show discounts field
		String[] discounts = Person.allowedDiscountType;
		Listing.displayChoices(discounts);
		message = "Should you apply a discount ? Select the type of discount.";
		String discount = discounts[UserEntries.getUserEntryByScan(message, discounts.length)-1];
		
		// register this person
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setEmail(email);
		this.setPhoneNumber(phoneNumber);
		this.setDiscount(discount);
		this.setStatus(status);
	}

	/**
	 * Find a person within a collection by lastName and Firstname
	 * 
	 * @param lastName
	 * @param firstName
	 * @param collection
	 * @return Person personFound
	 */
	public static Person find(String lastName, String firstName, Set<Person> collection) {
		
		Person personFound = null;
		boolean found = false;
		Iterator<Person> iterator = collection.iterator();
		
		while(iterator.hasNext() && !found) {
			Person person = iterator.next();
			if(person.getFirstName().equals(firstName) && person.getLastName().equals(lastName)) {
				personFound = person;
				found = true;
			}
		}
		
		return personFound;
	}
}
