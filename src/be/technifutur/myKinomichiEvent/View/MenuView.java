package be.technifutur.myKinomichiEvent.View;

public class MenuView{

	public static void show() 
	{
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=========================");
		System.out.println("==== KINOMICHI EVENT ====");
		System.out.println("=========================");
		System.out.println("                              ");
		System.out.println("1. Add registration");
		System.out.println("2. Update registration");
		System.out.println("3. Remove registration");
		System.out.println("4. List registration");
		System.out.println("5. Schedule Activity");
		System.out.println("6. Update scheduled Activity");
		System.out.println("7. Remove scheduled Activity");
		System.out.println("8. Lists scheduled activities");
		System.out.println("0. Quitter");
		System.out.println("                              ");
		System.out.println("=========================");
		System.out.println("                        ");
		System.out.println("                        ");
	}
}
