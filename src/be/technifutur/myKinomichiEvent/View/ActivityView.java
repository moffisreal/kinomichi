package be.technifutur.myKinomichiEvent.View;

import be.technifutur.myKinomichiEvent.Collection.ActivityCollection;
import be.technifutur.myKinomichiEvent.Model.Activity;
import be.technifutur.myKinomichiEvent.Utils.Listing;

/**
 * 
 * @author studentsc02
 *
 */
public class ActivityView {

	public static void showAddedSuccess() {
		System.out.println("                        ");
		System.out.println("=======================");
		System.out.println("You successfuly added an activity!");
		System.out.println("=======================");
		System.out.println("                        ");
	}

	public static void showUpdatedSuccess(String name, Activity activity) {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=======================");
		System.out.println(String.format("IMPORTANT : Its new name is now: %1$s", name));
		
		if(!name.equals(activity.getName())) {
			System.out.println(String.format("IMPORTANT : Its new name is now: %1$s", activity.getName()));
		}
		
		System.out.println("=======================");
		System.out.println("                        ");
		System.out.println("                        ");
	}
	
	public static void showDeletedSuccess(String name) {
		System.out.println("                        ");
		System.out.println("=======================");
		System.out.println("You successfuly deleted activity "+"\""+name+"\"");
		System.out.println("=======================");
		System.out.println("                        ");
	}

	public static void showScheduledCollection() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=======================");
		System.out.println("=== Activities list ===");
		System.out.println("=======================");
		Listing.displayActivities(ActivityCollection.getActivityScheduledCollection());
		System.out.println("=======================");
		System.out.println("                        ");
		System.out.println("                        ");
	}

	public static void showScheduleActivity() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=====================");
		System.out.println("=== Add activity  ===");
		System.out.println("=====================");
		System.out.println("                        ");
		System.out.println("                        ");
	}

	public static void showUpdateChoices() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=======================");
		System.out.println("=== Update Activity ===");
		System.out.println("=======================");
		System.out.println("1. Update user list");
		System.out.println("2. Add activity in user list");
		System.out.println("=======================");
		System.out.println("                        ");
		System.out.println("                        ");
	}
}
