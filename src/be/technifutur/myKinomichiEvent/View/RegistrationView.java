package be.technifutur.myKinomichiEvent.View;

import java.util.ArrayList;
import java.util.Map;

import be.technifutur.myKinomichiEvent.Model.Activity;
import be.technifutur.myKinomichiEvent.Model.Person;
import be.technifutur.myKinomichiEvent.Utils.Listing;

/**
 * 
 * @author studentsc02
 *
 */
public class RegistrationView {

	public static void addedSuccess() {
		System.out.println("                        ");
		System.out.println("----");
		System.out.println("You successfuly added a registration!");
		System.out.println("----");
		System.out.println("                        ");
	}
	
	public static void deletedSuccess() {
		System.out.println("                        ");
		System.out.println("----");
		System.out.println("You successfuly deleted a registration!");
		System.out.println("----");
		System.out.println("                        ");
	}

	public static void showWelcomeHeader() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("==========================");
		System.out.println("=== Add a registration ===");
		System.out.println("==========================");
		System.out.println("                        ");
		System.out.println("                        ");
	}
	
	public static void showPersons(Map<Person, ?> registrations) {
		Listing.displayPersons(registrations);
	}

	public static void showUpdateRegistration() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=============================");
		System.out.println("=== Update a registration ===");
		System.out.println("=============================");
		System.out.println("                        ");
		System.out.println("                        ");
	}
	
	public static void updatedSuccess() {
		System.out.println("                        ");
		System.out.println("----");
		System.out.println("You successfuly updated a registration!");
		System.out.println("----");
		System.out.println("                        ");
	}

	public static void showUpdateChoices() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=============================");
		System.out.println("======  UPDATE ENTRY  =======");
		System.out.println("=============================");
		System.out.println("1. Add new entry");
		System.out.println("2. Update existing entry");
		System.out.println("=============================");
		System.out.println("                        ");
		System.out.println("                        ");
	}
	
	public static void showListChoices() {
		System.out.println("                        ");
		System.out.println("                        ");
		System.out.println("=============================");
		System.out.println("======  UPDATE ENTRY  =======");
		System.out.println("=============================");
		System.out.println("1. All registrations");
		System.out.println("2. Registration by person");
		System.out.println("=============================");
		System.out.println("                        ");
		System.out.println("                        ");
	}

	public static void showRegistrationOfPerson(Map<Person, ArrayList<Activity>> registrations) {
		// TODO Auto-generated method stub
		Listing.displayRegistrationOfPerson(registrations, false);
	}
	
	public static void showRegistrations() {
		// TODO Auto-generated method stub
		Listing.displayRegistrations();
	}
}
