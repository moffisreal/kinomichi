package be.technifutur.myKinomichiEvent.Menu;

import be.technifutur.myKinomichiEvent.Model.Registration;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;
import be.technifutur.myKinomichiEvent.Model.Activity;
import be.technifutur.myKinomichiEvent.Model.Person;
import be.technifutur.myKinomichiEvent.View.ActivityView;
import be.technifutur.myKinomichiEvent.View.RegistrationView;

import be.technifutur.myKinomichiEvent.Collection.ActivityCollection;
import be.technifutur.myKinomichiEvent.Collection.RegistrationCollection;

import java.util.Map;

import be.technifutur.myKinomichiEvent.EntryPoint;

/**
 * 
 * @author studentsc02
 *
 */
public class Menu {
	
	public static final int MAX_MENU_SIZE = 9;
	
	/**
	 * Redirect user according to his choice
	 * 
	 * @param Int choice
	 */
	public static void redirectUser(int choice)
	{
		Map<Person, ?> registrations;
		Registration registration;
		Activity activity;
		String name, message;
		Person person;

		switch(choice) {
			case 1: // Add registration
				
				RegistrationView.showWelcomeHeader();
				registration = new Registration();
				try{
					registration.create();
					// save dev data
					RegistrationCollection.save();
					RegistrationView.addedSuccess();
				}
				catch(Exception e) {
					System.out.println(e.getMessage());
				}
				
				break;
			case 2: // Update registration
				
				RegistrationView.showUpdateRegistration();
				registration = new Registration();
				registrations = RegistrationCollection.getRegistrations();
				person = RegistrationCollection.askPersonInRegistrations(registrations);
				
				try{
					registration.update(person);
					RegistrationView.updatedSuccess();
				}
				catch(Exception e) {
					System.out.println(e.getMessage());
				}

				break;
			case 3: // Remove registration
				
				// TODO
				System.out.println("TODO : remove registration");
				
				break;
			case 4: // List registration for a person
				
				RegistrationView.showListChoices();
				int listRegistrationChoice = UserEntries.getUserEntryByScan("Select your choice:", 2);
				switch(listRegistrationChoice) {
					case 1:
						RegistrationView.showRegistrations();
						break;
					case 2:
						registrations = RegistrationCollection.getRegistrations();
						person = RegistrationCollection.askPersonInRegistrations(registrations);
						RegistrationView.showRegistrationOfPerson(RegistrationCollection.getRegistrationByReference(person));
						break;
				}
					
				break;
			case 5: // Schedule Activity
				
				ActivityView.showScheduleActivity();
				activity = new Activity();
				activity.schedule();
				// save dev data
				ActivityCollection.save();
				ActivityView.showAddedSuccess();
				
				break;
			case 6: //  Update Activity
				
				ActivityView.showScheduledCollection();
				message = "Enter the name of the activity you want to update:";
				name = ActivityCollection.askNameInScheduled(message);
				activity = ActivityCollection.getScheduledByName(name);
				activity.updateScheduled();
				ActivityView.showUpdatedSuccess(name, activity);
				
				break;
			case 7: // Delete activity
				
				ActivityView.showScheduledCollection();
				message = "Enter the name of the activity you want to remove:";
				name = ActivityCollection.askNameInScheduled(message);
				boolean result = ActivityCollection.deleteScheduled(name);
				if(result) {
					ActivityView.showDeletedSuccess(name);
				}
				else {
					System.out.println("-- Deletion cancelled -- ");
				}
				
				break;
			case 8: // Show scheduled activities collection
				
				ActivityView.showScheduledCollection();
				
				break;
			case 0:
				
				System.out.print("Bye, see ya!");
				
				break;
		}
		
		if(choice != 0) {
			EntryPoint.goToMenu();
		}
	}
}
