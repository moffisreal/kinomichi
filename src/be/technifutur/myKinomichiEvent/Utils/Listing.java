package be.technifutur.myKinomichiEvent.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import be.technifutur.myKinomichiEvent.Collection.RegistrationCollection;
import be.technifutur.myKinomichiEvent.Model.Activity;
import be.technifutur.myKinomichiEvent.Model.Person;

/**
 * 
 * @author studentsc02
 *
 */
public class Listing {

	/**
	 * Can be used to display menu list
	 * 
	 * @param choices
	 */
	public static void displayChoices(String[] choices) {
		for (int i = 0; i < choices.length; i++) {
			System.out.println(String.format("%1$s. %2$s", (i + 1), choices[i]));
		}
	}

	/**
	 * Can be used to display map items
	 * 
	 * @param choices
	 */
	public static void displayChoices(Map<String, ?> choices) {
		for (String string : choices.keySet()) {
			System.out.println(String.format("- %1$s", string));
		}
	}

	/**
	 * Can be used to display map with person as key
	 * 
	 * @param choices
	 */
	public static void displayPersons(Map<Person, ?> registrations) {
		for (Person person : registrations.keySet()) {
			System.out.println(String.format("- %1$s %2$s", person.getLastName(), person.getFirstName()));
		}
	}

	/**
	 * Added complete display of activities scheduled
	 * 
	 * @param activityScheduledCollection
	 */
	public static void displayActivities(Map<String, Activity> activityScheduledCollection) {
		Map<String, Activity> activities = activityScheduledCollection;
		System.out.println("                              ");
		for (Entry<String, Activity> entry : activities.entrySet()) {
			System.out.println(String.format("# %1$s > prix/unité: € %2$.2g | quantité max acceptée: %3$d", entry.getKey(),
					entry.getValue().getPrice(), entry.getValue().getMaxQuantity()));
		}
		System.out.println("                              ");
	}

	/**
	 * Display registration for a reference person
	 * 
	 * @param Map<Person, ArrayList<Activity>> registrations of a reference person
	 */
	public static void displayRegistrationOfPerson(Map<Person, ArrayList<Activity>> registrations, boolean isInSubMenu) {
		
		Map<Person, ArrayList<Activity>> activities = registrations;
		
		if(!isInSubMenu) {
			System.out.println("========================");
		}
		
		System.out.println("                        ");
		System.out.println("------------------------");
		System.out.println("      Réservation      ");
		System.out.println("------------------------");
		
		for (Entry<Person, ArrayList<Activity>> entry : activities.entrySet()) {
			System.out.println("------------------------");
			System.out.println(String.format("Activités de %1$s %2$s", entry.getKey().getLastName(), entry.getKey().getFirstName()));
			System.out.println("------------------------");
			for (Iterator<Activity> iterator = entry.getValue().iterator(); iterator.hasNext();) {
				Activity activity = iterator.next();
				System.out.println(
						String.format("%1$s > Quantité : %2$d au prix total de: € %3$.2g ",
								activity.getName(),
								activity.getQuantity(),
								activity.getPrice() * activity.getQuantity())
						);
			}
			System.out.println("------------------------");
			
		}
		System.out.println("========================");
		System.out.println("                        ");
	}
	
	/**
	 * Display all registrations
	 * 
	 * @param Map<Person, Map<Person, ArrayList<Activity>>> registrations
	 */
	public static void displayRegistrations() {
		
		Map<Person, Map<Person, ArrayList<Activity>>> registrations = RegistrationCollection.getRegistrations();
		
		System.out.println("==========================");
		System.out.println("== Toutes les commandes ==");
		System.out.println("==========================");
		
		for (Entry<Person, Map<Person, ArrayList<Activity>>> entry : registrations.entrySet()) {
			Person person = entry.getKey();

			System.out.println(
				String.format("Mr/Mme %1$s %2$s :",
						person.getLastName(),
						person.getFirstName()
						)
				);
			displayRegistrationOfPerson(entry.getValue(), true);
		}
	}
}
