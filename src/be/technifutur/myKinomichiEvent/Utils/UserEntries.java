package be.technifutur.myKinomichiEvent.Utils;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import be.technifutur.myKinomichiEvent.Model.Person;

public class UserEntries {

	public static Scanner scanner = new Scanner(System.in);
	private static String entry;

	/**
	 * Get user scan for Strings only, with parameter isMandatory
	 * 
	 * @param message
	 * @param isMandatory
	 * @return
	 */
	public static String getUserEntryByScan(String message, boolean isMandatory) {

		// user choice scan
		if (isMandatory) {
			do {
				System.out.println(message);
				UserEntries.entry = UserEntries.scanner.nextLine();
				if (UserEntries.entry.isEmpty()) {
					System.out.println("ERROR : The value is mandatory, please re type.");
				}
			} while (UserEntries.entry.isEmpty());
		} else {
			System.out.println(message + "- (press enter to leave it empty)");
			UserEntries.entry = UserEntries.scanner.nextLine();
		}

		return UserEntries.entry.isEmpty() ? " " : UserEntries.entry;
	}

	/**
	 * Get user entry by scan, only for integers
	 * > used for menu (1. xxx, 2.xxx) or retreiving a quantity
	 * 
	 * @param message
	 * @param maxMength
	 * @return int choice
	 */
	public static int getUserEntryByScan(String message, int maxLength) {
		int choice = 0;

		// user choice scan
		do {
			System.out.println(message);
			UserEntries.entry =  UserEntries.scanner.nextLine();

			// ensure user do not add a too long int
			if(Util.isNumeric(UserEntries.entry)) {
				choice = Integer.parseInt(UserEntries.entry);
			}
			else {
				System.out.println("ERROR : This is not a numeric value.");
			}	
			
			if (choice < 0 || choice > maxLength) {
				System.out.println("ERROR : The value is not permitted. Maximum value : "+ maxLength);
			}
		} while ( choice < 0 || choice > maxLength || ! Util.isNumeric(UserEntries.entry) );

		return choice;
	}
	
	/**
	 * Get user entry by scan, only for float.
	 * 
	 * @param message
	 * @return
	 */
	public static float getUserEntryByScan(String message) {
		float choice = 0;
		// user choice scan
		do {
			System.out.println(message);
			UserEntries.entry =  UserEntries.scanner.nextLine();

			if(Util.isNumeric(UserEntries.entry)) {
				UserEntries.entry.replace(',','.'); // TODO : fail
				choice = Float.parseFloat(UserEntries.entry);
			}
			else {
				System.out.println("ERROR : This is not a numeric value.");
			}
			
		} while ( choice < 0 || ! Util.isNumeric(UserEntries.entry) );

		return choice;
	}
	
	/**
	 * Get user entry by scan and force to use one of Map key values as value
	 * @param message
	 * @param Map collection <String, ?>
	 * @return Object Key
	 */
	public static String getUserEntryByScan(String message, Map<String, ?> collection) {
		
		do {
			System.out.println(message);
			UserEntries.entry =  UserEntries.scanner.nextLine();
			if (UserEntries.entry.isEmpty() || UserEntries.entry.isBlank()) {
				System.out.println("ERROR : Empty value not allowed.");
			}
			else if(! collection.containsKey(UserEntries.entry)) {
				System.out.println("ERROR :This is a not an existing value.");
			}
		} while (UserEntries.entry.isEmpty() || UserEntries.entry.isBlank() || ! collection.containsKey(UserEntries.entry));
		
		return UserEntries.entry;
	}
	
	/**
	 * Its a modal like, for the user to accept or reject something.
	 * @return
	 */
	public static boolean getUserEntryByScan() {
	
		System.out.println("Type ANYTHING and press ENTER to confirm,");
		System.out.println("or hit directly ENTER to cancel");
		
		boolean result = true;
		UserEntries.entry =  UserEntries.scanner.nextLine();
		if (UserEntries.entry.isEmpty()) {
			result = false;
		}
		
		return result;
	}
	
	/**
	 * Get user entry by scan and force to use one of Map key Person value as value
	 * @param message
	 * @param Set collection <Person>
	 * @return Object Key
	 */
	public static Person getUserEntryByScanForPerson(String message, Set<Person> collection) {
		
		Person person = null;
		
		do {
			System.out.println(message);
			UserEntries.entry =  UserEntries.scanner.nextLine();

			int separator = UserEntries.entry.trim().indexOf(' ');

			String lastName = UserEntries.entry.substring(0, separator);
			String firstName = UserEntries.entry.substring(separator+1);
			
			person = Person.find(lastName, firstName, collection);

			if (UserEntries.entry.isEmpty() || UserEntries.entry.isBlank()) {
				System.out.println("ERROR : Empty value not allowed.");
			}
			else if(person == null) {
				System.out.println("ERROR :This is a not an existing value.");
			}
		} while (UserEntries.entry.isEmpty() || UserEntries.entry.isBlank() || person == null);
		
		return person;
	}
}
