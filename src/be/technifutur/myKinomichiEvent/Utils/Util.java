package be.technifutur.myKinomichiEvent.Utils;

public class Util {
	public static boolean isNumeric(String strNum) {
	    return strNum.matches("-?\\d+(\\.\\d+|\\,\\d+)?");
	}
}
