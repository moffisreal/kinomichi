package be.technifutur.myKinomichiEvent.Collection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import java.util.TreeMap;

import be.technifutur.myKinomichiEvent.Model.Activity;
import be.technifutur.myKinomichiEvent.Model.Person;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;
import be.technifutur.myKinomichiEvent.View.RegistrationView;

/**
 * 
 * @author studentsc02
 *
 */
public class RegistrationCollection {

	private static Map<Person, Map<Person, ArrayList<Activity>>> registrations = new TreeMap<>(); // TODO ACCept to only
																									// register other
																									// ppl
	private Map<Person, ArrayList<Activity>> registration;

	public static Map<Person, Map<Person, ArrayList<Activity>>> getRegistrations() {
		return registrations;
	}

	public Map<Person, ArrayList<Activity>> getRegistration() {
		return registration;
	}

	public void setRegistration(Map<Person, ArrayList<Activity>> registration) {
		this.registration = registration;
	}

	public void createRegistrationCollection(Person person, ArrayList<Activity> activityCollection) {
		Map<Person, ArrayList<Activity>> registration = new TreeMap<>();
		this.setRegistration(registration);
		this.addActivitiesToPerson(person, activityCollection);
	}

	public void addActivitiesToPerson(Person person, ArrayList<Activity> activityCollection) {
		this.getRegistration().put(person, activityCollection);
	}

	public static void addToRegistrations(Person person, Map<Person, ArrayList<Activity>> registration) {
		getRegistrations().put(person, registration);
	}

	/**
	 * Retreive a registration with the referent person
	 */
	public static Map<Person, ArrayList<Activity>> getRegistrationByReference(Person person) {

		return getRegistrations().get(person);
	}

	public static Person askPersonInRegistrations(Map<Person, ?> registrations) {

		RegistrationView.showPersons(registrations);
		Person entryPerson = UserEntries.getUserEntryByScanForPerson("Enter 'LastName Firstname' in the list: ",
				registrations.keySet());

		return entryPerson;
	}

	/**
	 * Return a list of registrations TODO : a terminer
	 */
	public static void listRegistrations() {

		Map<Person, Map<Person, ArrayList<Activity>>> map = getRegistrations();
		for (Entry<Person, Map<Person, ArrayList<Activity>>> entry : map.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
		}
	}

	/**
	 * Load registration previously created
	 */
	@SuppressWarnings("unchecked")
	public static void load() {
		File file = new File("registrations.save");
		if (file.exists()) {
			try {
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
				registrations = (Map<Person, Map<Person, ArrayList<Activity>>>) in.readObject();
				in.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			registrations = new TreeMap<Person, Map<Person, ArrayList<Activity>>>();
		}
	}

	/**
	 * Save registrations
	 */
	public static void save() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File("registrations.save")));
			out.writeObject(registrations);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Check if user exists in a registration
	 * 
	 * @param firstName
	 * @param lastName
	 * @throws Exception 
	 */
	public static void registrationExists(String firstName, String lastName) throws Exception {
		
		registrations = RegistrationCollection.getRegistrations();
		for (Entry<Person, Map<Person, ArrayList<Activity>>> entry : registrations.entrySet()) {
			Person person = entry.getKey();
			if(person.getLastName().equals(lastName) && person.getFirstName().equals(firstName)) {
				throw new Exception("Error : User already exists for a registration");
			}
		}
	}

}