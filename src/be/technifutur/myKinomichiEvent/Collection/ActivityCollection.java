package be.technifutur.myKinomichiEvent.Collection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import be.technifutur.myKinomichiEvent.Model.Activity;
import be.technifutur.myKinomichiEvent.Model.Person;
import be.technifutur.myKinomichiEvent.Utils.UserEntries;
import be.technifutur.myKinomichiEvent.View.ActivityView;

/**
 * 
 * @author studentsc02
 *
 */
public class ActivityCollection {

	private static Map<String, Activity> activityScheduledCollection = new TreeMap<>();
	private ArrayList<Activity> activityCollection;
	
	public static Map<String, Activity> getActivityScheduledCollection() {
		return activityScheduledCollection;
	}
	
	public ArrayList<Activity> getActivityCollection() {
		return this.activityCollection;
	}
	
	public void setActivityCollection(ArrayList<Activity> activityCollection) {
		this.activityCollection = activityCollection;
	}
	
	public static void addScheduled(String name, Activity activity) {
		// store this activity
		getActivityScheduledCollection().put(name, activity);
	}
	
	public static void updateScheduled(String oldName, String newName, Activity activity) {
		
		Map<String, Activity> activities = ActivityCollection.getActivityScheduledCollection();
		
		// remove activity if name changed			
		if(!oldName.equals(newName)) {
			activities.remove(oldName);
		}
		
		// store this activity
		activities.put(newName, activity);
	}
	
	public static boolean deleteScheduled(String name) {
		
		System.out.println("Are you sure you want to delete this activity?");
		boolean result = UserEntries.getUserEntryByScan();
		
		if(result){
			 getActivityScheduledCollection().remove(name);
		}
		
		return result;
	}
	
	public static String askNameInScheduled(String message) {
		return UserEntries.getUserEntryByScan(message, getActivityScheduledCollection());
	}
	
	public static Activity getScheduledByName(String name) {
		return  getActivityScheduledCollection().get(name);
	}
	
	public void add(Activity activity) {
		this.getActivityCollection().add(activity);
	}
	
	public void update(Activity oldActivity, Activity activity) {
		
		remove(oldActivity);
		this.getActivityCollection().add(activity);
	}
	
	public void remove(Activity activity) {
		
		this.getActivityCollection().remove(activity);
	}

	/**
	 * Create a list of activities to add in
	 * 
	 * @return ArrayList<Activity> activityCollection 
	 */
	public void createActivityCollection() {

		ArrayList<Activity> activityCollection = new ArrayList<>();
		this.setActivityCollection(activityCollection);
		
		boolean doItAgain;
		String name, message;
		Activity activity, newActivity;

		do {
			ActivityView.showScheduledCollection();
	
			message = "Enter the name of the activity you want (according to the list)";
			name = ActivityCollection.askNameInScheduled(message);
			activity = ActivityCollection.getScheduledByName(name);

			message = "How much hour do you want? Price/unit =" + activity.getPrice();
			int quantity = UserEntries.getUserEntryByScan(message, activity.getMaxQuantity());

			newActivity = Activity.createFromScheduledActivity(activity, quantity);
			this.add(newActivity);
			
			// ask user if he wants to add another activity
			doItAgain = UserEntries.getUserEntryByScan();
			
		}while(doItAgain);
	}
	
	/**
	 * Load registration previously created
	 */
	@SuppressWarnings("unchecked")
	public static void load() {
		File file = new File("activities.save");
		if(file.exists()) {
			try {
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
				activityScheduledCollection = (Map<String, Activity>) in.readObject();
				in.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		else{
			activityScheduledCollection = new TreeMap<String, Activity>();
		}
	}
	
	/**
	 * Save registrations
	 */
	public static void save() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File("activities.save")));
			out.writeObject(activityScheduledCollection);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
